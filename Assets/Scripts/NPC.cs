﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class NPC : MonoBehaviour
{
    private Animator anim;
    private SoundPlayer sound;

    private GameObject hud;
    private GameObject gun;

    private GameObject platform;
    private GameObject level;

    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        sound = GetComponentInChildren<SoundPlayer>();

        hud = GameObject.Find("Canvas");
        gun = GameObject.Find("Gun");
        platform = GameObject.Find("Cube");
        level = GameObject.Find("Level");


        hud.SetActive(false);
        gun.SetActive(false);
        platform.SetActive(false);
        level.SetActive(false);

    }

    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") {
            hud.SetActive(true);
            gun.SetActive(true);
            platform.SetActive(true);
            level.SetActive(true);
        }
    }

}