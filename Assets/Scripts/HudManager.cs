﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudManager : MonoBehaviour
{

    private int actualShots;
    public Text ammoCounter;
    private bool reload;
    public Image reloBar;
    private float filler = 1;
    void Start()
    {

    }

    void Update()
    {
        actualShots = GameObject.Find("Player").GetComponent<BallShot>().currentAmmo;

        ammoCounter.text = "12/ " + actualShots.ToString();

        reload = GameObject.Find("Player").GetComponent<BallShot>().isReloading;


        if (reload == true)
        {
            filler = filler + Time.deltaTime * 300;
            reloBar.rectTransform.sizeDelta = new Vector2(filler, 15f);

        }
        if (reload == false)
        {
            reloBar.rectTransform.sizeDelta = new Vector2(0f, 15f);
            filler = 0;
        }
    }
}