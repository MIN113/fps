﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BallShot : MonoBehaviour
{

    public GameObject bullet_prefab;
    public GameObject canon;
    public GameObject pistol;
    float bulletImpulse = 2000f;
    public GameObject iniPos;
    public GameObject aimPos;
    public ParticleSystem gunparticle;

    public AudioSource shot;
    public AudioSource reload;

    public int maxAmmo;
    public int currentAmmo;
    public float fireRate;
    public float hitForce;

    private bool isShooting;
    public bool isReloading;

    public float reloadTime;

    private float recoil = 0.0f;
    private float maxRecoil_x = -20f;
    private float maxRecoil_y = 20f;
    private float recoilSpeed = 2f;
    private Transform cam;
    public Recoil recoilComponent;

    private GameObject end;

    private void Start()
    {
        isShooting = false;
        isReloading = false;
        currentAmmo = maxAmmo;
        cam = GameObject.FindWithTag("MainCamera").transform;
        end = GameObject.Find("End");
    }

    public void Shot()
    {
        if (isShooting || isReloading) return;
        if (currentAmmo <= 0) return;

        Debug.Log("Shoot");

        isShooting = true;
        currentAmmo--;

        recoilComponent.StartRecoil(0.05f, 25f, 60f);

        GameObject thebullet = (GameObject)Instantiate(bullet_prefab, canon.transform.position + canon.transform.forward, canon.transform.rotation);
        thebullet.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);

        GetComponent<ParticleSystem>();
        gunparticle.Play();

        shot.Play();

        StartCoroutine(WaitFireRate());
    }

    public void PistolPos()
    {
        pistol.transform.position = aimPos.transform.position;;
        
       
    }
    public void Aim()
    {
        pistol.transform.position = iniPos.transform.position;
        
    }

    private IEnumerator WaitFireRate()
    {
        Debug.Log("Empieza la corutina");
        yield return new WaitForSeconds(fireRate);
        isShooting = false;
        Debug.Log("Termina la corutina");
    }

    public void Reload()
    {
        if (isReloading) return;
        isReloading = true;

        reload.Play();

        StartCoroutine(WaitForReload());
    }
    private IEnumerator WaitForReload()
    {
        yield return new WaitForSeconds(reloadTime);
        currentAmmo = maxAmmo;
        isReloading = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "End")
        {
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }
    }
}