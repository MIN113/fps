﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSInputManager : MonoBehaviour
{

    private PlayerMove playerController;
    private float sensitivity = 3.0f;
    private LookRotation lookRotation;
    private Vector2 inputAxis;
    private Vector2 mouseAxis;
    private Laser laser;
    private BallShot ballS;
    private MouseCursor cursor;
    private bool reload;

    private GameObject pause;
    private bool paused;


    void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMove>();
        lookRotation = playerController.GetComponent<LookRotation>();

        //laser = playerController.GetComponent<Laser>();
        ballS = playerController.GetComponent<BallShot>();

        cursor = new MouseCursor();
        cursor.HideCursor();

        pause = GameObject.Find("Pause");
        pause.SetActive(false);

        //playerController = GameObject.FindGameObjectWithTag("Player2").GetComponent<PlayerMove2>();

    }

    void Update()
    {
        //El movimiento del player
        inputAxis.x = Input.GetAxis("Horizontal");
        inputAxis.y = Input.GetAxis("Vertical");
        playerController.SetAxis(inputAxis);
        //El salto del player
        if (Input.GetButton("Jump")) playerController.StartJump();

        //Rotación de la cámara
        mouseAxis.x = Input.GetAxis("Mouse X") * sensitivity;
        mouseAxis.y = Input.GetAxis("Mouse Y") * sensitivity;
        //Debug.Log("Mouse X = " + mouseAxis.x + " Y = " + mouseAxis.y);

        lookRotation.SetRotation(mouseAxis);
        if (Input.GetMouseButtonDown(0)) ballS.Shot();
        if (Input.GetKeyDown(KeyCode.R)) ballS.Reload();

        if (GameObject.Find("Player").GetComponent<BallShot>().currentAmmo == 0) {
            ballS.Reload();
        }

        if (Input.GetMouseButtonDown(1)) ballS.PistolPos();
        if (Input.GetMouseButtonUp(1)) ballS.Aim();

        if (Input.GetButtonDown("Pause")) {
            /*Time.timeScale = 0;
            pause.SetActive(true);
            cursor.ShowCursor();*/
            paused = !paused;
        }
        if (paused) {
            Time.timeScale = 0;
            pause.SetActive(true);
            cursor.ShowCursor();
        }
        if (!paused)
        {
            Time.timeScale = 1;
            pause.SetActive(false);
            cursor.HideCursor();
        }


        //if (Input.GetMouseButtonDown(0)) cursor.HideCursor();
        //else if (Input.GetKeyDown(KeyCode.Escape)) cursor.ShowCursor();
    }

        public void Resume()
         {
                paused = false;
         }

        public void Exit()
          {
                Application.Quit();
          }

}