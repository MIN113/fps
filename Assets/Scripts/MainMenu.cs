﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    private GameObject main;
    private MouseCursor cursor;

    void Start () {
        main = GameObject.Find("Canvas");
        cursor = new MouseCursor();
        cursor.ShowCursor();
    }


    public void Play()
    {
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
        cursor.HideCursor();
    }

    public void Exit()
    {
        Application.Quit();
    }
}
