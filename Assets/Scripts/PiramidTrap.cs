﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PiramidTrap : MonoBehaviour
{
    //private SoundPlayer sound;
    private GameObject piramid;
    private GameObject npc;

    private GameObject level;
    private GameObject end;

    public bool isTrue;

    void Start()
    {
        //        sound = GetComponentInChildren<SoundPlayer>();
        isTrue = false;

        level = GameObject.Find("Level2");

        piramid = GameObject.Find("PiramidInterior");
        npc = GameObject.Find("NPC");
        end = GameObject.Find("End");

        level.SetActive(false);
    }

    void Update()
    {
        if (isTrue == true)
        {
            piramid.transform.Translate(-Vector3.up * Time.deltaTime * 2, Space.World);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            npc.transform.Rotate(88, 0, 0, Space.World);
            level.SetActive(true);
            isTrue = true;
            end.transform.position = new Vector3 (270.7516f, -8.5f, 220.76f);
        }
    }

}